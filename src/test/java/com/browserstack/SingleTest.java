package com.browserstack;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.List;

public class SingleTest extends BrowserStackTestNGTest {

    @Test
    public void test() throws Exception {
      AndroidElement searchElement = (AndroidElement) new WebDriverWait(driver, 30).until(
          ExpectedConditions.elementToBeClickable(MobileBy.AccessibilityId("Search Wikipedia")));
      searchElement.click();
      AndroidElement insertTextElement = (AndroidElement) new WebDriverWait(driver, 30).until(
          ExpectedConditions.elementToBeClickable(MobileBy.id("org.wikipedia.alpha:id/search_src_text")));
      insertTextElement.sendKeys("BrowserStack");
      Thread.sleep(5000);

      List<AndroidElement> allProductsName = driver.findElementsByClassName("android.widget.TextView");
      Assert.assertTrue(allProductsName.size() > 0);
    }
}

/*
curl -u "nithyamani3:P4JKysg5WuchQxBfKQu1" -X POST "https://api-cloud.browserstack.com/app-automate/upload" -F "file=@/Users/nithyamani/Desktop/LocalSample.apk"
*/

//"app":"bs://<HASH_ID>" in "capabilities"
//this can be added in the *.conf.json file if we want to explicitly specify the hash ID by running the CURL command on terminal
//instead of passing by variable or code.
//to run this remove the 'else' part of the code -> 'Code Upload mechanism'
