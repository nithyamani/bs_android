package com.browserstack;
import com.browserstack.local.Local;
import java.io.*;
import java.io.BufferedReader;

import java.net.URL;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.io.FileReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import org.openqa.selenium.remote.DesiredCapabilities;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Parameters;


public class BrowserStackTestNGTest {
    public AndroidDriver<AndroidElement> driver;
    private Local l;

    @BeforeMethod(alwaysRun=true)
    @org.testng.annotations.Parameters(value={"config", "environment"})
    public void setUp(String config_file, String environment) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject config = (JSONObject) parser.parse(new FileReader("src/test/resources/conf/" + config_file));
        JSONObject envs = (JSONObject) config.get("environments");

        DesiredCapabilities capabilities = new DesiredCapabilities();

        Map<String, String> envCapabilities = (Map<String, String>) envs.get(environment);
        Iterator it = envCapabilities.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
        }
        
        Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
        it = commonCapabilities.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if(capabilities.getCapability(pair.getKey().toString()) == null){
                capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
            }
        }

        String username = System.getenv("BROWSERSTACK_USERNAME");
        if(username == null) {
            username = (String) config.get("user");
        }

        String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
        if(accessKey == null) {
            accessKey = (String) config.get("key");
        }

        String app = System.getenv("BROWSERSTACK_APP_ID");
        if(app != null && !app.isEmpty()) {
            System.out.println("APP_ID Set by Environment variable");
          capabilities.setCapability("app", app);
        }else {
            // the file we want to upload
            System.out.println("APP_ID Set By Code Upload mechanism");
            File inFile = new File("/Users/nithyamani/Desktop/APPS/WikipediaSample.apk");
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(inFile);
                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());

                // server back-end URL
                HttpPost httppost = new HttpPost("https://nithyamani3:P4JKysg5WuchQxBfKQu1@api.browserstack.com/app-automate/upload");
                MultipartEntity entity = new MultipartEntity();

                // set the file input stream and file name as arguments
                entity.addPart("file", new InputStreamBody(fis, inFile.getName()));
                httppost.setEntity(entity);

                // execute the request
                HttpResponse response = httpclient.execute(httppost);

                //int statusCode = response.getStatusLine().getStatusCode();
                HttpEntity responseEntity = response.getEntity();
                String responseString = EntityUtils.toString(responseEntity, "UTF-8");

                //System.out.println(responseString);
                String hash_id = responseString.substring(12, responseString.length() - 2);
                System.out.println(hash_id);
                capabilities.setCapability("app", hash_id);

            } catch (ClientProtocolException e) {
                System.err.println("Unable to make connection");
                e.printStackTrace();
            } catch (IOException e) {
                System.err.println("Unable to read file");
                e.printStackTrace();
            } finally {
                try {
                    if (fis != null) fis.close();
                } catch (IOException e) {
                }
            }
        }
        
        //enable local via code bindings
        if(capabilities.getCapability("browserstack.local") != null && capabilities.getCapability("browserstack.local") == "true"){
            l = new Local();
            Map<String, String> options = new HashMap<String, String>();
            options.put("key", accessKey);
            l.start(options);
        }

        driver = new AndroidDriver(new URL("http://"+username+":"+accessKey+"@"+config.get("server")+"/wd/hub"), capabilities);
    }

    @AfterMethod(alwaysRun=true)
    public void tearDown() throws Exception {
        driver.quit();
        if(l != null) l.stop();
    }
}
